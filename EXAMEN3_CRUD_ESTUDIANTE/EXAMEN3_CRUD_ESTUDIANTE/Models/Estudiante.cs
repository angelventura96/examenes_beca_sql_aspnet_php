﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EXAMEN3_CRUD_ESTUDIANTE.Models
{
    public class Estudiante
    {
        [Key]
        [Display(Name = "ID Estudiante")]
        public int Estudianteid { get; set; }

        [Required(ErrorMessage = "Se requeire el nombre del estudiante")]
        [Display(Name = "Nombre Estudiante")]
        public string Nombreestudiante { get; set; }

        [Required(ErrorMessage = "Se requeire el apellido del estudiante")]
        [Display(Name = "Apellido Estudiante")]
        public string Apellidoestudiante { get; set; }

        [Required(ErrorMessage = "Se requeire la edad del estudiante")]
        [Range(18,25)]
        [Display(Name = "Edad Estudiante")]
        public int Edadestudiante { get; set; }

        [Required(ErrorMessage = "Se requeire el correo del estudiante")]
        [EmailAddress(ErrorMessage = "El formato de correo ingresado no es valido")]
        [Display(Name = "Correo Estudiante")]
        public string Correoestudiante { get; set; }
    }
}
