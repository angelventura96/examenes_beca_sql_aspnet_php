﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EXAMEN3_CRUD_ESTUDIANTE.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace EXAMEN3_CRUD_ESTUDIANTE.Controllers
{
    public class EstudianteController : Controller
    {

        private readonly ApplicationDbContext _db;

        public EstudianteController(ApplicationDbContext db)
        {
            _db = db;
        }

        public IActionResult Index()
        {
            var mostrarDatos = _db.Estudiante.ToList();
            return View(mostrarDatos);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(Estudiante nuevoEstudiante)
        {
            if(ModelState.IsValid)
            {
                _db.Add(nuevoEstudiante);
                await _db.SaveChangesAsync();
                return Redirect("Index");
            }
            return View(nuevoEstudiante);
        }

        public async Task<IActionResult> Detail(int? id)
        {
            if(id == null)
            {
                return RedirectToAction("Index");
            }
            var detalleEstudiante = await _db.Estudiante.FindAsync(id);
            return View(detalleEstudiante);
        }

        public async Task<IActionResult> Edit (int? id)
        {
            if(id == null)
            {
                return RedirectToAction("Index");
            }
            var detalleEstudiante = await _db.Estudiante.FindAsync(id);
            return View(detalleEstudiante);
        }

        [HttpPost]
        public async Task<IActionResult> Edit (Estudiante estudianteEditado)
        {
            if (ModelState.IsValid)
            {
                _db.Update(estudianteEditado);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(estudianteEditado);
        }

        public async Task<IActionResult> Delete (int? id)
        {
            if(id == null)
            {
                return RedirectToAction("Index");
            }
            var detalleEstudiante = await _db.Estudiante.FindAsync(id);
            return View(detalleEstudiante);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            var detalleEstudiante = await _db.Estudiante.FindAsync(id);
            _db.Estudiante.Remove(detalleEstudiante);
            await _db.SaveChangesAsync();
            return RedirectToAction("Index");
        }
    }
}
