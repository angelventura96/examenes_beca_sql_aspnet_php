using System;
using System.Reflection;

namespace EXAMEN3_REST_CRUDEstudiante.Areas.HelpPage.ModelDescriptions
{
    public interface IModelDocumentationProvider
    {
        string GetDocumentation(MemberInfo member);

        string GetDocumentation(Type type);
    }
}