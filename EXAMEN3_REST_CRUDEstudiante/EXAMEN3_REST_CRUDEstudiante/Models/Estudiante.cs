//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EXAMEN3_REST_CRUDEstudiante.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Estudiante
    {
        public int Estudianteid { get; set; }
        public string Nombreestudiante { get; set; }
        public string Apellidoestudiante { get; set; }
        public Nullable<int> Edadestudiante { get; set; }
        public string Correoestudiante { get; set; }
    }
}
