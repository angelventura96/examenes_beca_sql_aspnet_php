﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EXAMEN3_REST_CRUDEstudiante.Models
{
    [MetadataType(typeof(Estudiante.MetaData))]
    public partial class Estudiante
    {
        sealed class MetaData
        {
            [Key]
            public int Estudianteid;

            [Required]
            public string Nombreestudiante;

            [Required]
            public string Apellidoestudiante;

            [Required]
            [Range(18,40)]
            public Nullable<int> Edadestudiante;

            [Required]
            [EmailAddress]
            public string Correoestudiante;
        }
    }
}