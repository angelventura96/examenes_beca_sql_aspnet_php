﻿using System.Web;
using System.Web.Mvc;

namespace EXAMEN3_REST_CRUDEstudiante
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
