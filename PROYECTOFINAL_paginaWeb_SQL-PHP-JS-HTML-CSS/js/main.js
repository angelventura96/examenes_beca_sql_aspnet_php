document.addEventListener("DOMContentLoaded", recuperarLS , bannerTimer(), getTextohome(), getProductos(), getServicios());

function recuperarLS(){
  var localstorage = window.localStorage;
  var icon = document.getElementById("theme");


  if(localstorage["tema"] == "sun"){
    //console.log("ENTRO AL IF SI EL LS ES  SUN");
    icon.setAttribute("src","https://img.icons8.com/color/96/000000/sun.png");
    icon.setAttribute("value","light");
    icon.setAttribute("alt","sun");
    document.getElementById("theme_css").setAttribute("href","css/main.css");
    localstorage.setItem("tema", icon.alt);
    //console.log(localstorage);
  }
  else{
    //console.log("ENTRO AL ELSE SI EL LS ES  MOON");
    icon.setAttribute("src","https://img.icons8.com/plasticine/100/000000/bright-moon.png");
    icon.setAttribute("value","dark");
    icon.setAttribute("alt","moon");
    document.getElementById("theme_css").setAttribute("href","css/darktheme.css");
    localstorage.setItem("tema", icon.alt);
    //console.log(localstorage);
  }
}

function aplicarZoom(){
  $("").mlens({
    imgSrc: $(this).attr("data-big"),
    lensShape: "circle",
    lensSize: 180,
    borderSize: 1,
    borderColor: "#0b3861",
  });
}

function cambiaColor(){

  var localstorage = window.localStorage;
	var icon = document.getElementById("theme");
   
 	if(icon.alt == "sun"){
 	 	icon.setAttribute("src","https://img.icons8.com/plasticine/100/000000/bright-moon.png");
      icon.setAttribute("value","dark");
      icon.setAttribute("alt","moon");
      document.getElementById("theme_css").setAttribute("href","css/darktheme.css");
      localstorage.setItem("tema", icon.alt);
      console.log(localstorage);
	}
	else{
        icon.setAttribute("src","https://img.icons8.com/color/96/000000/sun.png");
        icon.setAttribute("value","light");
        icon.setAttribute("alt","sun");
        document.getElementById("theme_css").setAttribute("href","css/main.css");
        localstorage.setItem("tema", icon.alt);
        console.log(localstorage);
	}
}

function bannerTimer(){
  setInterval(function(){
    $('#bannerdiv input')
       .eq( ( $('input:checked').index() + 1 ) % 4 )
       .prop( 'checked', true );
  },7000);
}

function enviarDatos(){
  var nombre = $("#name").val();
  var correo = $("#email").val();
  $.ajax({
    type: 'POST',/*tipo de request*/
    url: 'php/suscribir_newsletter.php',/*ruta/nombre del archivo al que se le manda el request*/
    data: { 
      sus_nombre: nombre, 
      sus_correo: correo 
    },/*datos que le envias a php*/
    success: function(dat){
      console.log(dat);
      $("#modal-text").html(dat);/*cargas la respuesta en la emergente*/
      $("#modal").css("display", "block");/*haces visible el div emergente*/
      setTimeout(function(){ 
          $("#modal").css("display", "none");
          document.getElementById("name").value = "";
          document.getElementById("email").value = "";},
          "2000");
    }
  });
}

function getTextohome(){
  $.ajax({
    type: 'GET',
    url: 'php/consulta_textohome.php',
    data: {},
    success: function(dat){

      var jsondat = JSON.parse(dat); //string dat to objecttype json
      var contenedor = document.querySelector('#contenedor_home');
      contenedor.innerHTML = '';

      for(let texto of jsondat){ //cada producto

        contenedor.innerHTML += `
        <div class="espacio-arriba-grande text-center col-md-8">
            <h3>${texto[1]}</h3>
            <p class="centrar-padding espacio-arriba-mediano">${texto[2]}</p>
          </div>
        `
      }
    }
  });
}

function getProductos(){
  $.ajax({
    type: 'GET',
    url: 'php/consulta_productos.php',
    data: {},
    success: function(dat){
      var jsondat = JSON.parse(dat); //string dat to objecttype json
      var contenedor = document.querySelector('#contenedor_productos');
      contenedor.innerHTML = '';

      for(let producto of jsondat){ //cada producto
        contenedor.innerHTML += `
        <div class="col-md-4 inline-block quitar-formato-link-azul espacio-arriba-grande">
          <img class="img-zoom zoom" src="${producto[4]}" data-big="${producto[4]}">
          <div class="hover">
            <h4 class="espacio-arriba-pequeño">${producto[1]}</h4>
            <p>${producto[2]}</p>
            <p class="text-right"><a href=${producto[3]}>Ver más...</a></p>
          </div
        </div>
        <br>
        `
      }
      aplicarZoom();
    }
  });
}

function getServicios(){
  $.ajax({
    type: 'GET',
    url: 'php/consulta_servicios.php',
    data: {},
    success: function(dat){

      var jsondat = JSON.parse(dat); //string dat to objecttype json
      var contenedor = document.querySelector('#contenedor_servicios');
      contenedor.innerHTML = '';

      for(let servicio of jsondat){ //cada producto

        contenedor.innerHTML += `
        <div class="col-md-4 inline-block quitar-formato-link-azul espacio-arriba-grande">
          <img src="${servicio[4]}">
          <div class="hover">
            <h4 class="espacio-arriba-pequeño">${servicio[1]}</h4>
            <p>${servicio[2]}</p>
            <p class="text-right"><a href="${servicio[3]}">Ver más...</a></p>
          </div>
        </div>
        <br>
        `
      }
    }
  });
}

function getParametroURL(parametroNombre){
  var url = window.location.search.substring(1);
  var urlVariables = url.split('&');
  for (var i = 0; i < urlVariables.length; i++) {
    var parametro = urlVariables[i].split('=');
    if (parametro[0] == parametroNombre) {
      itemNombre = parametro[1];

      var contenedor_titulo_item = document.querySelector('#titulo_item'); //h3 del titulo de la categoria de producto
      contenedor_titulo_item.innerHTML = itemNombre; //inserta el nombre de la categoria de los productos que se visualizaran

      getItemsProdServ(itemNombre);
      //return parametro[1];
    }
  }
  return null;
}

//funcion para obtener cada uno de los productos o servicios desde la bd 
function getItemsProdServ(parametroNombre){
  $.ajax({
    type: 'POST',
    url: 'php/consulta_items.php',
    data: {
      nombre_tabla : parametroNombre //PLAYERA
    },
    success: function(dat){

      var jsondat = JSON.parse(dat); //string dat to objecttype json
      var contenedor = document.querySelector('#contenedor_item');
      contenedor.innerHTML = '';

      for(let item of jsondat){ //cada producto
        itemLenght = item.length - 1; //length para las imagenes
        contenedor.innerHTML += `
        <div class="inline-block col-md-4">
          <div class="galeria col-centered lupa">
            <input type="radio" name="${item[0]}" id="_1" checked>
            <input type="radio" name="${item[0]}" id="_2">
            <input type="radio" name="${item[0]}" id="_3">
            <input type="radio" name="${item[0]}" id="_4"> 
            <img src="${item[itemLenght - 3]}" width="180" height="180" alt=""/>
            <img src="${item[itemLenght -2]}" width="180" height="180" alt=""/>
            <img src="${item[itemLenght - 1]}" width="180" height="180" alt=""/>
            <img src="${item[itemLenght]}" width="180" height="180" alt=""/>
          </div>
          <div class="hover">
            <h4 class="espacio-arriba-mediano">${item[2]}</h4> <!--NOMBRE DEL PRODUCTO/SERVICIO-->
            <br>
            <div id="id${item[0]}"></div>
          </div
        </div>
        `
        var contenedor_descripcion = document.querySelector('#id'+item[0]);

        item_descripcion = item.slice(3,-4); //NUEVO ARREGLO SIN ID,IDPROD,NOMBRE, Y LOS 4 IMGLINKS

        contenedor_descripcion.innerHTML = '';
        
        for (let data of item_descripcion) {
          contenedor_descripcion.innerHTML += `
            <p><span class="font-bold">${data.split(":")[0]}: </span>${data.split(":")[1]}</p>
          `
        }
      }
      aplicarZoom();
    }
  });
}