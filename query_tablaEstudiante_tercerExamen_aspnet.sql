create database EXAMEN3

use EXAMEN3

CREATE TABLE Estudiante(
	Estudianteid int not null primary key identity(1,1),
	Nombreestudiante nvarchar(150),
	Apellidoestudiante nvarchar(150),
	Edadestudiante int,
	Correoestudiante nvarchar(150)
);

insert into Estudiante(Nombreestudiante, Apellidoestudiante, Edadestudiante, Correoestudiante)
values ('Jose Angel', 'Ventura Perez', 24, 'javentura96@gmail.com');
insert into Estudiante(Nombreestudiante, Apellidoestudiante, Edadestudiante, Correoestudiante)
values ('Karla Fabiola', 'Ventura Perez', 22, 'kfventura98@gmail.com');

select * from Estudiante;