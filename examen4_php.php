1. Explica que hace el siguiente código y cual seria el resultado del script.

            <?php
                $global = 'global';
                $global2 = 2;
                $global3 = 3;

                function prueba4(){
                $GLOBALS['global2'] = $GLOBALS['global2'] + $GLOBALS['global3'];
                echo $GLOBALS[‘global2’];
                }

                prueba4();
            ?>

R = Define 3 variables globales (fuera de la función), pero las manipula dentro de la función con la super variable $GLOBALS
    que es un array que contiene todas las variables globales, la salida del script sería: 5

---------------------------------------------------------------------------------------------------------------------------------

2. Explica que es la palabra reservada STATIC, para que sirve y cual sería la salida del siguiente código.
            <?php
                function pruebaStatic() {
                  static $x = 0;
                  echo 'dentro de la funcion'.$x;
                  $x++;
                }

                pruebaStatic();
                echo "<br>";
                pruebaStatic();
                echo "<br>";
                pruebaStatic();
            ?>

R = STATIC nos sirve para preservar los valores de la variable $x, al finalizar la funcion, por tanto, al hacer las ejecuciones
    de la función en las lineas consecuentes, la primera vez será 0, la segunda ejecucion sera 1, esto por que al finalizar la
    primera ejecucion el valor final de x es 1, y la tercera ejecucion sera 2, por la misma razon, al final de la segunda ejecucion
    el valor de x es 2, y conservamos el valor.

-----------------------------------------------------------------------------------------------------------------------------------

3. Codifica las 4 opciones que tenemos en php de operadores de incremento/decremento.

R =   <?php

      $var = 10; //definimos la variable $var con el valor de 10

      //pre-incremento : primero se realiza el incremento al valor de la variable y despues se muestra en pantalla
      echo ++$var; // la salida sera 11

      //post-incremento : primero se imprime el valor de la variable y despues, se realiza el incremento,
      echo $var++.'<br>'; //La salida sera 11
      echo $var; //la salida sera 12

      //predecremento : primero se realiza el decremento al valor de la variable y despues se muestra en pantalla
      echo --$var; //la salida sera 11

      //post-decremento : primero se imprime el valor de la variable y despues, se realiza el decremento,
      echo $var--.'<br>'; //la salida sera 11
      echo $var; //la salida sera 10
      ?>

------------------------------------------------------------------------------------------------------------------------------------------

4. Explica los conceptos con un ejemplo de array indexado, asociativo y multidimensional.

R = <?php
      //Los arrays indexados son arrays a los cuales accedemos a sus elementos mediante la posición numérica de su index.
      $datos = array(“hola”,”adios”); // $datos es un array de 2 elementos y los índices serian 0 para hola y 1 para adios.

      //Los arrays asociativos tienen el formato clave-valor para cada elemento y accedemos a sus elementos mediante el nombre de la clave.
      $datos2 = array(“nombre”=>”Fernando”, “edad”=>24); //tenemos dos elementos, nombre=fernando, y edad=24, las claves serian nombre y edad,
                                                         //y los valores serían fernando y 24.

      /*Los arrays multidimensionales son arrays que contienen más arrays, el número de dimensiones estará dado por el número de índices que
      necesitamos para llegar a un valor concreto del array,*/
      $carros2 = array ( array("Volvo",22,18),
                         array("BMW",15,13),
                         array("Saab",5,2)
                       );
    ?>

--------------------------------------------------------------------------------------------------------------------------------------------------

5. ¿Cual seria la salida del siguiente script?
      <?php
          $carros2 = array (  array("Volvo",22,18),
                              array("BMW",15,13),
                              array("Saab",5,2)
                           );

          echo $carros2[0][0].": stock: ".$carros2[0][1].", vendidos: ".$carros2[0][2].".<br>";
          echo $carros2[1][0].": stock: ".$carros2[1][1].", vendidos: ".$carros2[1][2].".<br>";
          echo $carros2[2][0].": stock: ".$carros2[2][1].", vendidos: ".$carros2[2][2].".<br>";
          echo $carros2[3][0].": stock: ".$carros2[3][1].", vendidos: ".$carros2[3][2].".<br>";
      ?>

R = La salida sería Volvo, stock: 22, vendidos: 18
		                BMW, stock: 15, vendidos: 13
		                Saab, stock: 5, vendidos: 2

---------------------------------------------------------------------------------------------------------------------------------------------------

6. ¿Qué hace el siguiente script?

    <?php
        $carros4 = array (
                          array("Volvo",22,18),
                          array("BMW",15,13),
                          array("Saab",5,2),
                          array("Land Rover",17,15)
                         );

        for ($array_interno = 0; $array_interno < count($carros4); $array_interno++) {
          echo "<p><b>Array interno: $array_interno</b></p>";
          echo "<ul>";
          for ($info_array_interno = 0; $info_array_interno < count($carros4[$array_interno]); $info_array_interno++) {
            echo "<li>".$carros4[$array_interno][$info_array_interno]."</li>";
          }
          echo "</ul>";
        }
    ?>

R = Definimos un array bidimensional en la variable $carros4, cada elemento del arreglo, es también un arreglo, que tiene 3 valores, un string, y 2 enteros.
    iniciamos un ciclo for, donde inicializamos la variable $array_interno = 0 que será nuestro contador,
    el ciclo se ejecutará mientras el valor de $array_interno sea menor al número de elementos del array en la variable $carros4 (funcion count($carros4))
    el incremento de nuestro contador será de 1(++).
    Dentro del ciclo hacemos un echo del string “Array interno:” y concatenamos el valor de $array_interno, que es el índice numérico del elemento
    en turno dentro de $carros4 y ademas iniciamos otro ciclo for.
    Dentro del ciclo for, anidaremos otro ciclo for para recorrer los elementos de cada elemento de $carros4 (recordar que $carros4 tiene 4 elementos),
    volvemos a inicializar un contador en 0, y la condición será , ejecuta mientras contador sea menor al número de elementos de cada elemento de
    $carros4, y dentro de ese ciclo, hacemos echo de elementos de lista (<li>), con los valores del elemento.

--------------------------------------------------------------------------------------------------------------------------------------------------

7. Explica que hace el siguiente código.

    <?php
        $cont3 = 0;

        while($cont3 <= 10) {
            if($cont3 == 4 || $cont3 == 6) {
                $cont3++;
                continue;
            }
            elseif($cont3 == 9){
                break;
            }
            echo "Contador vale: $cont3 <br>";
            $cont3++;
        }
    ?>

R = Definimos e inicializamos una variable $cont3 con valor de 0
    iniciamos un ciclo while, la condición será mientras $cont3 sea menor o igual a 10,
    El bloque de código a ejecutarse contendrá una cláusula IF, donde evaluamos que cuando $cont3, sea igual a 4 o igual a 6,
    ejecutara una cláusula continue, por lo que escapara todo lod demás del bloque de codigo, y pasará a la siguiente iteración,
    si esta cláusula if es falsa, evalúa la cláusula ELSEIF, donde evaluará que cuando $cont3 sea igual a 9, ejecutará una cláusula break,
    saliendo por completo del ciclo while, si la condición IF, y la condición ELSEIF son falsas, imprimirá el valor de $cont3 y posteriormente
    incrementera su valor en 1 (++).
    Considerando las cláusulas continue y break, la salida del script sería: 1, 2, 3, 5, 7, y 8

-----------------------------------------------------------------------------------------------------------------------------------------------

8. Define una interfaz llamada Animal, con dos metodos, nombreAnimal() y hacerRuido(), define una clase concreta Perro que implemente esa interfaz, implementa los métodos.

R = <?php
      interface Animal{ //definimos la interfaz
          public function nombreAnimal(); //metodo abstracto (sin implementacion) nombreAnimal()
          public function hacerRuido();//metodo abstracto (sin implementacion) hacerRuido()
      }

      class Perro implements Animal{ //palabra reservada IMPLEMENTS para poder implementar los metodos abstractos de la interfaz

          public function nombreAnimal(){ //implementacion del metodo abstraccto nombreAnimal() de la interfaz Animal
              echo ‘Me llamo Blacky;
          }

          public function hacerRuido(){ //implementacion del metodo abstraccto hacerRuido() de la interfaz Animal
              echo ‘Ladrido Wooooof’;
          }
      }
    ?>

-------------------------------------------------------------------------------------------------------------------------------------------------

9. ¿Qué hace el siguiente código?

    <?php
        class Fruta {

            public $nombre;
            public $color;

            function set_nombre($nombre) {
                $this->nombre = $nombre;
            }
            function get_nombre() {
                return $this->nombre.' - Dentro de la funcion get_nombre';
            }
        }

        $manzana = new Fruta();
        $banana = new Fruta();

        $manzana->set_nombre('Manzana');
        $banana->set_nombre('Banana');

        echo $manzana->get_nombre();
        echo $banana->get_nombre();
    ?>

R = Definimos una clase Fruta, la cual posee 2 atributos, nombre y color, sin valores.
    También tiene 2 metodos set_nombre que recibe como parametro una variable y dicha variable la setea como valor del atributo nombre con la palabra
    reservada "this", y el segundo metodo es get_nombre que simplemente nos regresa el valor del atributo nombre del objeto.
    Después instanciamos 2 objetos de la clase Fruta, en las variables, manzana y banana
    Después a nuestro objeto de tipo fruta en la variable manzana, utilizamos su método set nombre, pasando como parámetro un string “Manzana”,
    hacemos lo mismo con banana.
    Luego imprimimos en pantalla los valores que asignamos con el método get_nombre de nuestros objetos.

------------------------------------------------------------------------------------------------------------------------------------------------

10. Explica el funcionamiento del siguiente formulario HTML

    <form class="logueo" action="ruta/archivo.php" method="POST">
      <input type="text" name="login" placeholder="Usuario"><br>
      <input type="password" name="pass" placeholder="Contraseña"><br>
      <button type="submit">Entrar</button><br><br>
    </form>

R = Es un formulario para enviar por metodo POST al archivo "archivo.php", dos variables, login y pass, ambas son obtenidas en inputs, login, es
    un input dde tipo text, y pass se obtiene con un input de tipo password.
    El tercer elemento sera un boton, que al ser presionado enviara los datos del formulario al archivo "archivo.php" (atributo action del form)

------------------------------------------------------------------------------------------------------------------------------------------------

11. Crea un enlace que direccione a un archivo llamado index.html que se encuentra 2 carpetas atrás de la ruta del archivo donde se encuentra el enlace.

R = <a href="../../index.html">Regresar a Index</a>

-----------------------------------------------------------------------------------------------------------------------------------------------

12. Crea una tabla html con 2 encabezados, el segundo encabezado deberá de contener tres filas (<td>)

R = <table> <!--Inicia la tabla-->
        <tr> <!--Cada tr es un tableRow, sera cada fila(horizontal)-->
            <th>Encabezado1</th> <!--th es tableheader, sera el encabezado superior-->
            <th colspan="3" style="text-align:center;">Encabezado2</th><!--segundo encabezado que abarcara dos celdas de la fila inferior-->
        </tr> <!--Se cierra la primera fila-->
        <tr><!--Inicia la segunda fila-->
            <td>Texto1</td><!--td es table data cell, es decir cada celda de la tabla-->
            <td>Texto2</td>
            <td>Texto3</td>
        </tr><!--termina la segunda fila-->
    </table><!--termina la tabla-->

---------------------------------------------------------------------------------------------------------------------------------------

13. Crea un formulario HTML en el cual se envíe un archivo con extensión .jpg

R = <form action="archivo_a_ejecutar.php" method="POST" enctype="multipart/form-data"> <!--enctype nos permite enviar multimedia-->
      <input type="file" name="archivo_multimedia_jpg" accept=".jpg"><!--la variable a enviar sera archivo_multimedia_jpg por POST-->
      <button type="submit">Enviar</button><br><br><!--Boton para enviar la variable al archivo del atributo action del form-->
    </form>

-----------------------------------------------------------------------------------------------------------------------------------------

14. Crea y documenta la estructura básica de un documento HTML

R = <html> <!--Inicia la etiqueta html para indicar que todo lo contenido dentro es codigo html, no es visible en la pagina-->
        <head><!--Aqui hacemos referencia a otros archivos que se utilizaran como archivos JS, archivos CSS, es la metadata del documento-->
          <meta charset="utf-8"><!--formato de codificacion de caracteres-->
          <title>TITULO DE LA PeSTAÑA EN EL BROWSER</title><!--titulo de la pestaña en el navegador-->
          <script type="text/javascript" src="js/jquery-3.5.1.min.js"></script><!--inclusion de libreria jquery-->
          <script type="text/javascript" src="js/main.js"></script><!--inclusion de un script JS-->
        </head>
        <body><!--Cuerpo del documento, es visible en la pagina, aqui va todo el codigo de nuestra pagina-->
            <p>TITULO</p> <!--Mostrara la palabra TITULO con la etiqueta parragraph-->
            <!--podemos utilizar las etiquetas <a, b, p, h1, h2, h3, table, section, div, etc>-->
        </body><!--Cierre del cuerpo del documento-->
        <footer><!--Pie de pagina de la pagina, pueden utilziarse las mismas etiquetas que en el body, simplementepor
                estructura se mostrara en la parte final de nuestra pagina-->
        </footer>
    </html><!--Etiqueta de cierre de la linea 271-->

----------------------------------------------------------------------------------------------------------------------------------------

15. Crea la estructura básica de un menú de navegación en HTML

R = <ul> <!--Etiqueta unordered list, contiene los elementos de la lista-->
      <li>Elemento del menu 1</li> <!--Etiqueta LI list item, elementos individuales de la lsita, añade formato de viñeta-->
      <li>Elemento del menu 2</li>
      <li>Elemento del menu 3</li>
    </ul> <!--Cierre de la etiqueta UL-->

-----------------------------------------------------------------------------------------------------------------------------------------

16. Cual es la función de la siguiente etiqueta? <img src="ruta/archivo.png">

R = Incluir en nuestro documento html, una imagen, que se encuentra en la carpeta ruta, y el archivo se llama archivo.padding

--------------------------------------------------------------------------------------------------------------------------------------

17. Crea un campo de tipo texto, en el cual por cada evento de teclado mande llamar a una función JavaScript llamada buscar, en
    el cual se envíe el evento de teclado y el objeto del campo.

R = <input type="text" onclick="buscar(eventoTeclado, this)">

---------------------------------------------------------------------------------------------------------------------------------------

18. Indica para que es usado un atributo de clase y un atributo id, pon un ejemplo de cada uno de ellos.

R = Las clases normalmente son utilizadas para dar estilos css al elemento que contiene la clase
    <p class="texto-centrado"></p>

    .texto-centrado{          //el punto al principio indica que sera asignado a una clase
      color: white;
      text-align:center;
    }

    b) Los atributos id son usados para  interactuar con dichos elementos con etiquetas a href, o desde codigo javascript por ejemplo.

    <a href="#enc1">Ir a encabezado 1</a> <!--Esto es para hacer links, dentro del mismo archivo, supongamos un header fijo donde este
    link este dentro un listitem (elemento de lista/menu) y todo el contenido esta en nuestra misma pagina haciendo scrooll, este link
    nos permitira navegar hasta el header1, sin hacer scroll-->

    <h1 id="enc1">Encabezado 1</h1> <!--Encabezado 1-->

    document.getElementById("enc1").value;

-----------------------------------------------------------------------------------------------------------------------------------------

19. Menciona los dos tipos de envío de datos a través de HTML

R = POST Y GET
