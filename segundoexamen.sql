1) Crea una función que retorne un REF CURSOR con los trabajadores de un departamento
dado y el nombre del departamento en el que trabaja.
/*
R:
	CREATE OR REPLACE FUNCTION get_emp(v_dept_id IN employees.department_id%TYPE)
	RETURN SYS_REFCURSOR
	AS
	   cursor_emp SYS_REFCURSOR;
	BEGIN
	   OPEN cursor_emp FOR 
	       SELECT employee_id,first_name,last_name,dep.department_name
	       FROM employees emp INNER JOIN departments dep
	       ON emp.department_id = dep.department_id
	       WHERE emp.department_id = v_dept_id
	       ORDER BY first_name,last_name;
	   RETURN cursor_emp;
	END;
*/

2) Para el ejemplo anterior, crea un bloque anónimo que obtenga el REF CURSOR con los
datos del departamento #30 y muestra los datos en DBMS
/*
R:
	DECLARE
	   cursor_emp SYS_REFCURSOR;
	   v_emp_id employees.employee_id%TYPE;
	   v_firstname employees.first_name%TYPE;
	   v_lastname employees.last_name%TYPE;
	   v_dept departments.department_name%TYPE;
	BEGIN
	   cursor_emp := get_emp(30); 
	   LOOP
	      FETCH cursor_emp INTO v_emp_id,v_firstname,v_lastname,v_dept;
	      EXIT WHEN cursor_emp%notfound;
	      dbms_output.put_line(v_emp_id||': '||v_firstname||' '||v_lastname||' - '||v_dept);
	   END LOOP;
	   CLOSE cursor_emp;
	END;
*/

3) ¿Qué hace el siguiente código?

	CREATE OR REPLACE TRIGGER salary_change_trigger
	AFTER UPDATE OF salary ON employees
	BEGIN
		INSERT INTO log_table (user_id, logon_date)
		VALUES (USER, SYSDATE);
	END;
/*
R:Crea un trigger llamado salary_change_trigger, que será ejecutado automáticamente después 
de que se lleve a cabo un UPDATE a la columna salary de la tabla employees, el trigger consistirá 
en insertar en la tabla log_table, el user_id del usuario que llevó a cabo la actualización, y la 
fecha en que fue realizada.

*/

4) ¿Qué hace el siguiente código?
	CREATE OR REPLACE TRIGGER employee_dept_fk_trg
	BEFORE UPDATE OF department_id ON employees
	FOR EACH ROW
	DECLARE
		v_dept_id departments.department_id%TYPE;
	BEGIN
		SELECT department_id INTO v_dept_id
		FROM departments
		WHERE department_id= :NEW.department_id;
		EXCEPTION
			WHEN NO_DATA_FOUND THEN
				INSERT INTO departments
				VALUES(:NEW.department_id, 'Dept'||:NEW.department_id, NULL, NULL);
	END;
/*
R:Este código crea un trigger nombrado employee_dept_fk_trg por que al actualizar la columna 
department_id de la tabla empleados, esta columna tiene un constraint de que el departamento 
debe existir.
el trigger se ejecutará ANTES de hacer la actualizacion y lo que hace es buscar el department_id 
que se va a actualizar en los departamentos existentes, si no lo encuentra, inserta el nuevo valor 
en la tabla departments , después de esto, la cláusula update podría ser ejecutada correctamente, 
por que para ese momento el departamento ya existe y es válido.
*/

5) Se necesita verificar que el salario de un trabajador al ser actualizado no sea menor a su
salario actual o que su salario nuevo no sea menor al salario mínimo. ¿Cómo podrías
hacerlo? Considera que año con año el salario mínimo será mayor, y necesitamos eficientar
este proceso. Codifica.
/*
R:Podemos crear una tabla simple llamada salariominimo, que contenga una sola columna salario_minimo 
que almacene el valor int del salario mínimo, de esta forma solo tendriamos que hacer un update a la 
columna salario_minimo cuando este valor cambie.

	CREATE TABLE salariominimo (salario_minimo NUMBER(8,2));
	INSERT INTO salariominimo (salario_minimo) VALUES (500);
	UPDATE salariominimo SET salario_minimo = 550;

y posteriormente crear nuestro trigger para el momento de la actualización por cada fila de la 
columna salario.

	CREATE OR REPLACE TRIGGER checar_salario
	BEFORE UPDATE OF salario ON empleados
	FOR EACH ROW
	DECLARE
		v_sal_min salariominimo%salario_minimo%TYPE;
	BEGIN
		SELECT salario_minimo INTO v_min_sal
		FROM salariominimo;
		IF :NEW.salario < v_sal_min
		OR :NEW.salario < :OLD.salario THEN
			RAISE_APPLICATION_ERROR (-20508,'No disminuir salario.');
		END IF;
	END;
*/

6) ¿Cuál sería la sintaxis para deshabilitar todos los triggers de una tabla?
/*
R: 	ALTER TABLE nombre_tabla DISABLE | ENABLE ALL TRIGGERS.
*/

7) Resuelve el siguiente problema.

	a) Escribe el código de un programa que cree un paquete declarando lo siguientes puntos:

	1.- Un cursor llamado "dep_cursor" el cual almacene los distintos departamentos de la tabla
	"employees" y ordenándolo por nombre del departamento
	2.- Un procedimiento para abrir el cursor llamado "open_cursor_depts"
	3.- Un procedimiento para cerrar el cursor llamado "close_cursor_depts"
	4.- Una función que extraiga las filas con el nombre de "fetch_n_rows" que tenga un
	parámetro de entrada tipo NUMBER y que regrese un BOOLEAN
/*
R:
		CREATE OR REPLACE PACKAGE test_pkg IS --DEFINICION DEL PAQUETE

			--PASO 1
		    CURSOR dep_cursor IS
			    SELECT DISTINCT(department_name)
			    FROM employees
		    ORDER BY department_name;

			--PASO 2
		    PROCEDURE open_cursor_depts;

		    --PASO 3
		    PROCEDURE close_cursor_depts;

		    --PASO 4
		    FUNCTION fetch_n_rows(param_in NUMBER) RETURN BOOLEAN;


		END test_pkg;

*/

	b) Crea el cuerpo del paquete y define los puntos declarados anteriormente considerando lo
	siguiente:

	1.- El procedimiento "open_cursor_depts" debe de asegurase de abrir el cursor
	2.- El procedimiento "close_cursor_depts" debe de asegurase de cerrar el cursor
	3.- La funcion "fetch_n_rows" debe de guardar en una variable los nombres de los
	departamentos e imprimirlos de una manera continua de acuerdo a el parámetro que recibe
	y regresar un valor BOOLEAN "dep_cursor%found"
/*
R:

CREATE OR REPLACE PACKAGE BODY test_pkg IS  -- CREACION DEL BODY DEL PAQUETE
    
    --PASO 1

    PROCEDURE open_cursor_depts IS
    BEGIN
        IF NOT dep_cursor%isopen 
        	THEN OPEN dep_cursor;  --%isopen devolvera un true o false, si devuelve true, IF NOT, lo niega y sería falso. por eso el THEN abre el cursor
        END IF;
    END open_cursor_depts; --termina la funcion


    --PASO2

    PROCEDURE close_cursor_depts IS
    BEGIN
        IF dep_cursor%isopen -- %isopen devolvera true o false
        THEN -- si &isopen devuelve true
            CLOSE dep_cursor; --cierra el cursor
        END IF;
    END close_cursor_depts; --termina la funcion


	--PASO 3

    FUNCTION fetch_n_rows (param_in NUMBER ) 
    RETURN BOOLEAN IS
        nombre_departamente employees.department_name%TYPE; --variable del tipo de la columna department_name de la tabla empleados

    BEGIN
        FOR count IN 1 .. param_in -- for desde la posicion 1 hasta el parametro que le pasemos a la funcion
        LOOP
            FETCH dep_cursor INTO nombre_departamento; --Pasamos la info del primer registrod el cursor a nuestra variable
            EXIT WHEN dep_cursor%notfound; --El loop termina despues del ultimo registro
            dbms_output.put_line('Department Name: ' ||(nombre_departamento));
        END LOOP;

        RETURN dep_cursor%found; -- nos indica si despues del numro de registros que especificamos que queriamos con param_in, aun quedan registros en el cursor
    END fetch_n_rows;

END test_pkg;
*/

	c) En un bloque anónimo manda llamar al paquete creado con un parámetro = 5.
/*
R:

	DECLARE
	    hay_filas BOOLEAN := true; -- Variable para saber cuadno salir del LOOP
	BEGIN
	    test_pkg.open_cursor_depts; -- llamamos al procedimiento open_cursor_depts del paquete test_pkg
	    LOOP
	        more_rows := test_pkg.fetch_n_rows(5); Llamamos al procedimiento fetch_n_rows y le pasamos el parametro 5
	        
	        dbms_output.put_line('-------');
	        EXIT WHEN NOT more_rows;
	    END LOOP;

	    test_pkg.close_cursor_depts;
END;
*/

8) ¿Qué hace el siguiente código?

	CREATE OR REPLACE PROCEDURE empl_status ( p_dir IN VARCHAR2, p_filename IN VARCHAR2 ) IS
		v_file utl_file.file_type;
		CURSOR empl_cursor IS
	SELECT first_name, last_name, manager_id, salary, department_id
	FROM employees
	ORDER BY employee_id;
	BEGIN
		v_file := utl_file.fopen(p_dir, p_filename, 'w');
		utl_file.put_line(v_file,'REPORT: GENERATED ON ' || sysdate ||' '|| 'EMPLOYEES
		INFORMATION');
		utl_file.new_line(v_file);
		FOR cursor_record IN empl_cursor LOOP
			utl_file.put_line(v_file, ' EMPLOYEE: ' || cursor_record.first_name ||
			cursor_record.last_name || 'works for: ' || cursor_record.manager_id ||
			'earns: ' || cursor_record.salary);
		END LOOP;
		utl_file.put_line(v_file, '*** END OF REPORT ***');
		utl_file.fclose(v_file);
		EXCEPTION
			WHEN utl_file.invalid_path THEN
				raise_application_error(-20003,'Invalide Path');
			WHEN utl_file.invalid_filehandle THEN
				raise_application_error(-20001, 'Invalid File.');
			WHEN utl_file.write_error THEN
				raise_application_error(-20002, 'Unable to write to file');
	END empl_status;

	BEGIN sal_status('Examen', 'employees_info.txt'); 
	END;
/*
R: Crea un archivo txt de un reporte de empleados donde se especifica el nombre del empleado y para quien trabaja
*/

9) Indica si el siguiente código está escrito correctamente, explica tu respuesta.

	DECLARE
		v_first_name VARCHAR2(20);
	BEGIN
		DECLARE
			v_last_name VARCHAR2(20);
		BEGIN
			v_first_name := 'Carmen';
			v_last_name := 'Miranda';
			dbms_output.put_line(v_first_name || ' ' || v_last_name);
		END;
		dbms_output.put_line(v_first_name || ' ' || v_last_name);
	END;
/*
R: No es correcto, ya que el primer bloque BEGIN no puede ver la variable v_last_name, ya que esta esta definida en el bloque interno
por lo tanto es local y el primer bloque no la puede ver.
*/

10) A continuación queremos imprimir el valor de todas las variables en el bloque interior.
¿Cómo podemos acceder a la variable v_date_of_birth si en el bloque interior ya hay un
variable con el mismo nombre? Escribe la parte faltante del código

	DECLARE
		v_father_name VARCHAR2(20) := 'Patrick';
		v_date_of_birth DATE := '20-Abril-1972';
	BEGIN
		DECLARE
			v_child_name VARCHAR2(20) := 'Mike';
			v_date_of_birth DATE := '12-Diciembre-2002';
		BEGIN
			dbms_output.put_line('Father''s Name: ' || v_father_name);
			dbms_output.put_line('Child''s Name: ' || v_child_name);
			dbms_output.put_line('Date of Birth: ' || v_date_of_birth);
		END;
	END;
/*
R: Aunque v_date_of_birth es una variable con alcance global para el bloque intermno, este tambien tiene una variable local con el mismo nombre,
para poder usar la variable del bloque exterior en el bloque interior, tendriamos que usar una etiqueta <<outer>> antes de la definicion del primer bloque,
y hacer referencia a la variable de ese bloque outer, como outer.v_date_of_birth desde el bloque interno.
*/

11) ¿Qué hace el siguiente código?

	DECLARE
		CURSOR cur_pais IS
			SELECT country_id, country_name
			FROM countries;
		v_country_id countries.country_id%TYPE;
		v_country_name countries.country_name%TYPE;
	BEGIN
		OPEN cur_pais; LOOP
		FETCH cur_pais INTO v_country_id,v_country_name;
		EXIT WHEN cur_pais%notfound;
			dbms_output.put_line(v_country_id|| ' '|| v_country_name);
		END LOOP;
		CLOSE cur_pais;
	END;
/*
R: Hace un qeury a la tabla countries, seleccionando las columnas country_id, y country_name, todos los registros de estas dos columnas son pasadas al cursor llamado cur_pais.
se declaran dos vairables, v_country_id y v_country_name, de los tipos de datos de las columnas country_id, y country_name de la tabla countries con %TYPE
Se abre el cursor, e inicia el loop
y pasara cada registro del cursor en las variables, v_country_id, v_country_name, imprimiendo cada registro en la salida dbms, 
el loop termina cuando no haya mas registros
se cierra el cursor
*/

13) Describa lo que el siguiente cursor hace, desde su consulta haciendo énfasis en la
cláusula WHERE y en el FOR LOOP.

	DECLARE
	CURSOR cur_emp_min_sal (p_countri VARCHAR2, p_depto NUMBER) IS
		SELECT be.employee_id, be.salary, d.department_name, c.country_name
		FROM backup_employees be LEFT OUTER JOIN departments d
		ON ( be.department_id = d.department_id )
		LEFT OUTER JOIN locations l
		ON ( d.location_id = l.location_id )
		LEFT OUTER JOIN countries c ON ( l.country_id = c.country_id )
		WHERE c.country_name = p_countri AND be.department_id = p_depto
		AND be.salary < ( SELECT AVG(salary)
		FROM backup_employees
		WHERE department_id = p_depto )
		FOR UPDATE OF salary NOWAIT;
	BEGIN
		FOR v_emp_min_sal IN cur_emp_min_sal('United States of America', 90)
		LOOP
			UPDATE backup_employees
			SET salary = v_emp_min_sal.salary * 1.5
			WHERE CURRENT OF cur_emp_min_sal;
		END LOOP;
	END;
/*
R: es un cursor que recibe dos parametros de entrada, p_country, y p_depto
contendra el query de las columnas employeeid, salary de la tabla backup_employees como 'be,
departmentname de la tabla departments como 'd'
countryname de la tabla countries como 'c'
PAra hacer el query tendra que verificar mediante los leftouterjoin que haya relacion entre backupemployees y departments en su campo id
ya con esa condicion cumplida, que la tabla departments y la tabla locations, tengan relacion en sus columnas location_id
con esa otra condicion cumplida, que la tabla locations y la tabla countries, tengan relacion en sus columnas country_id
Despues de cumplir todo esto, (son relaciones anidadas, donde la infromacion depende de muchas tablas), 
verifica que la columna countryname sea igual al valor que enviamos como parametro en la variable p_country, Y que el departmentid de la tabla backupemployee
sea igual al valor que pasamos como parametro en la variable p_depto
Y que el valor de la columna salary de la tabla backupemployees, sea menor, el promedio salarial del departamento que pasamos como parametro de la tabla backupemployeees
Despues inicia un for con los registros que contiene nuestro cursor, que recibe los parametros United States of America, y depto 90,
y actualizara la tabla backupemployees, modificando el salario, en turno en el loop, en un 50%
*/

14) ¿Qué realiza el siguiente código?

	CREATE OR REPLACE PROCEDURE query_country
		(p_tag IN countries.country_id%TYPE, 
		p_name OUT countries.country_name%TYPE,
		p_id OUT countries.region_id%TYPE ) 
		IS
	BEGIN
		SELECT country_name, region_id INTO p_name, p_id
		FROM countries WHERE country_id = p_tag;
	END query_country;

	DECLARE
		a_emp_name countries.country_name%TYPE;
		a_emp_id countries.region_id%TYPE;
	BEGIN
		query_country('MX', a_emp_name, a_emp_id);
		dbms_output.put_line('Country Name: ' || a_emp_name);
		dbms_output.put_line('Region ID: ' || a_emp_id);
	END;
/*
R:Define un procedimmiento llamado 'query_country' que recibe un parametro IN llamado p_tag, y dos parametros OUT llamados p_name y p_id. 
HAce un query a la tabla countries, regresando las columnas country_name, que sera asignada a nuestra variable out p_name, y la columna region_id, que sera
asignada a nuestra variable p_id.
De los registros que cumplan con la condicion: country_id igual a p_tag que es nuestro parametro IN (nosotros especficamos el id)
*/

15) Realiza y ejecuta un procedimiento para insertar filas a una columna en la tabla "jobs"
con los datos job_id, job_title, min_salary y max_salary.
/*
R:
	CREATE OR REPLACE PROCEDURE agregar_empleo IS
	    variable_job_id obs.job_id%TYPE;
	    variable_nombre_empleo jobs.job_title%TYPE;
	    variable_salario_minimo jobs.min_salary%TYPE;
	    variable_salario_maximo jobs.max_salary%TYPE;
	BEGIN
	    variable_job_id := 'BARR';
	    variable_nombre_empleo := 'Barrendero';
	    variable_salario_minimo :='1000';
	    variable_salario_maximo :='2000';
	    INSERT INTO jobs 
	    (job_id,
	    job_title,
	    min_salary,
	    max_salary
	    ) VALUES (
	    variable_job_id,
	    variable_nombre_empleo,
	    variable_salario_minimo, 
	    variable_salario_maximo 
	    );
	END;

	BEGIN
	agregar_empleo;
	END;
*/

16) Se desea borrar una tabla en momento de ejecución mediante el proceso
“drop_any_table”. Especifica que sentencia falta y en donde se debe colocar para que este
proceso funcione.

	CREATE PROCEDURE drop_any_table (p_table_name VARCHAR2) IS
	BEGIN
		DROP TABLE p_table_name;
	END;
/*
R:
	CREATE PROCEDURE drop_any_table(p_table_name VARCHAR2)
	IS 
	BEGIN
	EXECUTE IMMEDIATE 'DROP TABLE' || p_table_name; -- ESTA ES LA LINEA QUE FALTA, YA QUE SQL STANDAR NO PUEDE EJECTUAR LA LINEA DROP TABLE p_table_name;
	END;

EXECUTE IMMEDIATE: Sentencia de SQL dinamico, la cual permite ejecutar sentencias de SQL que tienen informacion variable, como en este caso la linea drop table,
donde el nomrbe de la tabla es variable dependiendo delo que reciba nuestra funcion drop_any_table.
*/

17) Crea una función que elimine todos los registros de cualquier tabla y devuelva el conteo
de dichos registros. Invoca la función borrando los registros de la tabla
“COPY_EMPLOYEES”
/*
R:
	CREATE FUNCTION eliminar_todos_registros ( param_nombretabla VARCHAR2)
	RETURN NUMBER IS 
	BEGIN
		EXECUTE IMMEDIATE 'DELETE FROM ' || param_nombre_tabla 
		RETURN SQL%ROWCOUNT; --devolvera el conteo de los registros afextados por la sentencia delete de la linea de arriba
	END;

	DECLARE
	variable_contador NUMBER; --se declara la variable variable_contador de tipo NUMBER
	BEGIN
		variable_contador := eliminar_todos_registros ('tabla_empleados'); -- a nuestra variable declarada, se le asigna el valor de regreso de la funcion eliminar_todos_registros
		DBMS_OUTPUT.PUT_LINE(variable_contador|| ' registros borrados.'); --muestra en salida DBMS el valor de vairable_contador, que sera el numero de registros eliminados
	END;
*/

18) Utilizando un record realiza una consulta donde se obtenga el nombre del empleado
(first_name) y el trabajo que tiene (job_title).
/*
R:
	DECLARE
		TYPE emp_inf IS RECORD (first_name employees.first_name%TYPE,job_title jobs.job_title%TYPE); -- se define un tipo de dato emp_inf, que sera un record con 2 columnas
																								     -- first_name del tipo de dato de la columna firstname de la tabla employees, 
																								     --y job_title, del tipo de dato de la columna job_title de la tabla jobs
		info_empleados_record emp_inf; -- se define una variable emp_info_rec del tipo que se definio anteriormente.
	BEGIN
		SELECT emp.first_name, job.job_title -- selecciona las columnas first name de 'emp' que es la tabla empleados
											 -- selecciona la columna jobtitle de 'job' que es la tabla jobs
		INTO info_empleados_record -- manda los registros a neustro record de tipo emp_inf
		FROM employees emp JOIN jobs job -- el select como ya se dijo, sera sobre la tabla de empleados (firstname), y la tabla jobs (jobtitle)
		ON emp.job_id = job.job_id -- se tomaran los registros donde el jobid de ambas tablas, empleados y jobs sea igual
		WHERE employee_id = 150; -- solo del empleado que tenga el id 150
			DBMS_OUTPUT.PUT_LINE('El empleado: '||' '||emp_info_rec.first_name ||' ' || ' trabaja  en: '|| ' ' ||emp_info_rec.job_title);
	END;
*/

19) ¿Qué hace el siguiente código?

	DECLARE
		TYPE tab_dep IS TABLE OF departments%ROWTYPE
		INDEX BY BINARY_INTEGER;
		rec_tab_dep tab_dep;
	BEGIN
		FOR dep_rec IN (SELECT*FROM departments) LOOP
			rec_tab_dep(dep_rec.department_id) := dep_rec;
			DBMS_OUTPUT.PUT_LINE(rec_tab_dep(dep_rec.department_id).department_name);
		END LOOP;
	END;
/*
R:Declara una tabla INDEX BY de tipo binario que toma en cuenta el id del departamento para almacenar los datos de todauna columna, dando como 
resultado los diferentes departamentos que existen registrados 
*/

20) ¿Cuál será el resultado del siguiente bloque de código?

	DECLARE
		e_myexcep EXCEPTION;
		e_myexcep2 EXCEPTION;
	BEGIN
		BEGIN
			BEGIN
				RAISE e_myexcep;
				dbms_output.put_line('Message 1 ');
			END;
			EXCEPTION
			WHEN e_myexcep THEN
					dbms_output.put_line('Message 2');
				RAISE e_myexcep2;
					dbms_output.put_line('Message 3');
		END;
	EXCEPTION
	WHEN e_myexcep2 THEN
		dbms_output.put_line('Message 4');
	END;
/*
R: La salida sería el mensaje 2 y el mensaje 4, por que, en el 3er bloque se lansa la excepcion e_myexcep, antes no se habia ejecutado nada,
y al lanzarse, la linea de mensaje 1, es omitida, y termina el 3er bloque, el segundo bloque interno, (segundo begin), cacha la excepcion, e imprime
el mensaje 2, al hacer esto, la linea 'raise e_myexcep2' es ejecutada, por tanto la salida dbms de mensjae 3 no es ejecutada, y termina el
segundo bloque, el primer bloque (primer BEGIN) cacha a e_myexcep, y ejecuta la salida DBMS de mensaje 4.
*/

21) Realice un bloque de código anónimo donde se le dé doble tratamiento a una excepción
causada por que un salario actual (20000) es mayor al salario máximo (10000). En el bloque
interno debe darse solo aviso de la excepción y en el bloque externo debe solucionarse el
problema.
/*
R:
	DECLARE
	  salario_elevado EXCEPTION; --excepcion llamada salario elevado
	  salario_actual NUMBER := 20000; --inicializamos con asignacion, la variable salario_Actual en 20000
	  salario_maximo NUMBER := 10000; -- inicializamos con asignacion la variable salario_maximo en 10000
	  salario_erroneo NUMBER; --definimos la variable salario_erroneo de tipo number
	BEGIN
	  BEGIN  
	    IF salario_actual > salario_maximo THEN --Si el salario actual es mayor al salario_maximo
	      RAISE salario_elevado;  -- lanza la excepcion salario_elevado
	    END IF;
	  EXCEPTION
	    WHEN salario-elevado THEN --aqui se cacha la excepcion pero solo se dice lo que esta sucediendo
	      DBMS_OUTPUT.PUT_LINE('Salario ' || salario erroneo || ' es muy grande'); --se muestra que el salario actual esta fuera de lo permitido
	      DBMS_OUTPUT.PUT_LINE('Salario maximo : ' || salario_maximo || '.'); -- se muestra cual es el valor del salario maximo permitido
	      RAISE;  
	  END;  
	EXCEPTION
	  WHEN salario_elevado THEN --aqui se trata la excepcion
	    salario_erroneo := salario_actual; --se asigna a la variable salario_erroneo el valor de salario actual que es 20000
	    salario_Actual := salario_maximo; --se asigna a la variable salario_actual el valor de salario maximo, que es 10000
	    DBMS_OUTPUT.PUT_LINE('Salario que se intento asignar ' || salario_erroneo || ' se asigno como: ' || salarioactual || '.'); 
	END;
*/

22) ¿Qué hace el siguiente código?

	DECLARE
		v_out NUMBER := 0;
		v_in NUMBER := 5;
	BEGIN
		<<loop_externo>> LOOP
			v_out := v_out + 1;
			v_in := 3;
			EXIT WHEN v_out > 4;
			<<loop_interno>> LOOP
				dbms_output.put_line('Numero de loops externos: '|| v_out|| ' con numero loop interno: '|| v_in);
				v_in := v_in - 1;
				EXIT WHEN v_in = 0;
			END LOOP loop_interno;
		END LOOP loop_externo;
	END;
/*
R: inicializa v_out en 0, y v_in en 3, despues comienza un loop donde el valor de v_out, es incrementado en 1, por lo tanto ahora v_out es igual a 1,
v_in es modificado a 3 , el loop terminara cuando v_out, sea mayor a 4, dentro del loop, mostrara en la salida DBMS, el numero de loops externos, que en la
primera ronda es 1, por que ese es el valor de v_out, y lo concatena con el numero de loops internos, que en la primera vuelta es 3, que es el valor de v_in,
posteriormente de la impresion DBMS, v_in, sera disminuido en 1, por tanto ahora, v_in, vale 2, y el loop interno terminara cunado v_in llegue a cero, 
en resumen, muestra las combinaciones de cada valor de v_out (1, 2, 3, 4), con todos los valores de v_in (3, 2, 1). (1 - 3, 1-2, 1-1, ...)
*/

23) Se requieren agregar 5 registros para la ciudad de Turquía a la tabla locations, las
direcciones y otros datos aún no han sido confirmados, pero se requiere la creación de los
id, inserta los registros basados en el último id y siendo el nombre de la ciudad Turquía,
haciendo uso de un while loop.
/*
R:
	DECLARE
		variable_id locations.location_id%TYPE; -- se declara la variable vairable_id del tipo de la columna location_id de la tabla locations
		variable_contador NUMBER := 1; --se declara la vairable variable_contador de tipo NUMBER inicializada por asignacion con valor 1
	BEGIN
	SELECT MAX(location_id) -- query del location_id mas grande, de la tabla locations
	INTO variable_id -- lo asigna a la vairable que definimos con %type
	FROM locations; -- de la tabla locations
	WHILE -- condicion
		variable_contador <=5   LOOP -- mientras la variable_contador se menor o igual a 5
			INSERT INTO locations(location_id, city) -- insertara en la tabla locations en las columnas, location_id, y city
			VALUES((variable_id+ variable_contador), 'Turquia'); -- los valores, de las dos variabels sumados (idmax + 1) que definimos en un principio, y turquia
			variable_contador := variable_contador + 1; -- se incrementa el valor de la variable contador en 1+
		END LOOP;
	END;
*/

24) Crea un procedimiento o función que de acuerdo a los primeros dos dígitos del número
telefónico del empleado diga la zona en la que vive, para visualizarlo, realizar un query para
ver el nombre de los empleados, su número telefónico y utiliza el procedimiento o función
anterior para visualizar la zona, como extra, ordenarlos por orden alfabético de acuerdo al
nombre de la zona.
/*
R:
	create or replace FUNCTION funcion_obtener_zona(param_telefono IN VARCHAR2)
	RETURN VARCHAR2 IS
	BEGIN
		CASE -- lo que evalua es el inicio del string telefono con el formato ('numero numero % ') numero es un string numerico, y % es 'lo que sea despues de dos string numericos
			-- los caracteres que evaluara seran 55.... , 95..., 77..., 86..., 34...'
		    WHEN param_telefono LIKE ('55%') THEN RETURN ('CDMX');
		    WHEN param_telefono LIKE ('95%') THEN RETURN ('OAXACA');
		    WHEN param_telefono LIKE ('77%') THEN RETURN ('PUEBLA');
		    WHEN param_telefono LIKE ('86%') THEN RETURN ('MTY');
		    WHEN param_telefono LIKE ('34%') THEN RETURN ('GDL');
		    ELSE RETURN ('No hay una zona registrada'); --si el telefono ingresado no hace match con las condiciones anteriores, rergesa este mensaje 'no hay zona registrada'
		 END CASE;
	END obtener_zona; --fin de la funcion

	SELECT columna_nombre, columna_telefono, obtener_zona(param_telefono)
	FROM tabla_empleados
	ORDER BY funcion_obtener_zona(param_telefono);

*/

25) ¿Qué es lo que hace el siguiente código?

	DECLARE
		nombre VARCHAR2(25);
		apellido VARCHAR2(25);
	BEGIN
		SELECT e.first_name,e.last_name
		INTO nombre,apellido
		FROM employees e
		WHERE e.first_name = 'Lex';
		dbms_output.put_line('Datos del empleado: '|| nombre|| ' '|| apellido);
		EXCEPTION
		WHEN too_many_rows THEN
			dbms_output.put_line('La consulta regresa mas de un resultado!!!');
	END;
/*
R: Es un bloque anonimo, el cual define dos variables de tipo varchar2(25), una para nombre, y una para apellido.
Realiza un select a la tabla empleados con el identificador 'e', y selecciona las columnas, firstname y lastname, 
con la condicion que el valor de la columna firstname sea igual a 'Lex', si esto es correcto, con UN SOLO REGISTRO, 
mostrara en la salida DBMS el nombre y apellido de LEX, si la consulta regresa mas de un registro, mostrara en la 
salida DBMS el mensaje 'La consulta regresa mas de un resultado'.
*/

26) Crea un bloque anónimo que actualice el manager_id de la tabla departments a 102 para
el departamento 190, y que actualice el job_id por SA_MAN y el manager_id por 102 a los
empleados del manager 121 o donde su job_id sea NULL.
/*
R:
	BEGIN 
    	UPDATE departments --ACTUALIZA LA TABLA DEPARTMENTS
    	SET manager_id = '102' 
    	WHERE department_id = 190; --DONDE EL DEPARTMENTID SEA 190
    	UPDATE employees --ACTUALIZA LA TABLA EMPLOYEES
        SET job_id = 'SA_MAN', 
            manager_id = 102
    	WHERE job_id IS NULL OR manager_id = '121';
	END;
*/
